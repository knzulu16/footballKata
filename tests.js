const assert = require('assert');


var team = require('./team.js');
var player = require('./player.js');
var match = require('./match.js');





describe('Team', function() {
  it("Should return which team and which player", function() {
    var Players = new team("Cowboys","Andile");

    assert.equal(Players.teamName,"Cowboys");
    assert.equal(Players.skeem,"Andile");

  })
});
describe('Player', function() {

  it("Should return the name,goals and the position of each player", function() {
    var playerStats=new player("Mark","md",9);
    assert.equal(playerStats.name,"Mark");
    assert.equal(playerStats.position,"md");
    assert.equal(playerStats.goal,9);

  });
});
describe('Match', function() {


  it("Should return which team and which player scored, at what time did the player scored", function() {

    var firstTeam=new match("17:30",2,"Cowboys");
    assert.equal(firstTeam.time,"17:30");
    assert.equal(firstTeam.goal,2);
    assert.equal(firstTeam.teamOne,"Cowboys");


  })
  it("Should return which team and which player scored, at what time did the player scored", function() {

    var SecondTeam=new match("15:15",1,"Chippa");

    assert.equal(SecondTeam.time,"15:15");
    assert.equal(SecondTeam.goal,1);
    assert.equal(SecondTeam.teamTwo,"Chippa");




  })


})
